package com.example.assignment1

fun main(){

    numberToWord(99)
    numberToWord(379)
    numberToWord(101)
    numberToWord(999)
    numberToWord(777)
    numberToWord(69)
    numberToWord(699)



}

fun numberToWord(numb:Int){

    val ones = arrayOf("", "ერთი", "ორი", "სამი", "ოთხი", "ხუთი",
                            "ექვსი", "შვიდი", "რვა", "ცხრა", "ათი",
                            "თერთმეტი", "თორმეტი", "ცამეტი", "თოთხმეტი",
                            "თხუთმეტი", "თექვსმეტი", "ჩვიდმეტი", "თვრამეტი", "ცხრამეტი")

    val tens = arrayOf("", "", "ოცი", "ოცდაათი", "ორმოცი", "ორმოცდაათი",
                                "სამოცი", "სამოცდაათი", "ოთხმოცი", "ოთხმოცდაათი")

    val anotherTens = arrayOf("", "", "ოცდა", "ოცდა", "ორმოცდა", "ორმოცდა",
                                "სამოცდა", "სამოცდა", "ოთხმოცდა", "ოთხმოცდა")

    val hundreds = arrayOf("", "ასი", "ორასი", "სამასი", "ოთხასი", "ხუთასი",
                            "ექვსასი", "შვიდასი", "რვაასი", "ცხრაასი", "ათასი")

    val anotherHundreds = arrayOf("", "ას ", "ორას ", "სამას ", "ოთხას ", "ხუთას ",
                            "ექვსას ", "შვიდას ", "რვაას ", "ცხრაას ")

    if (numb in 0..1000){
        if (numb in 1..19){
            println(ones[numb])
        }
        else if (numb in 20..99 step 10){
            println(tens[numb / 10])
        }
        else if (numb in 20..99){
            println(anotherTens[numb / 10] + ones[numb % 20])
        }
        else if (numb in 100..1000 step 100){
            println(hundreds[numb / 100])
        }
        else if (numb in 101..999){
            println(anotherHundreds[numb / 100] + anotherTens[(numb % 100) / 10] +  ones[numb % 20])
        }


    }else println("შეიყვანეთ რიცხვი რომელიც 0-იდან 1000-ის შუალედშია მოთავსებული!")
}
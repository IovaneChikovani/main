package com.example.assignment1

import kotlin.math.max

fun main(){

    findDifferent(arrayOf(5,5,5,5,5,6))
    msgavseba(arrayOf(1,5,9), arrayOf(1,23,5,8,9))
    gaertianeba(arrayOf(1,2,3,4,5), arrayOf(2,3,6,9))
    meotxeDav(arrayOf(1,2,3,4,9))
    mexuteDav(arrayOf(2,1,3,67,7))

}

fun findDifferent(numbs: Array<Int>){
    var difNumbs: MutableList<Int> = mutableListOf()

    for (each in numbs){

        if (difNumbs.isEmpty()){
            difNumbs.add(each)
        }

        else if (each !in difNumbs){
            difNumbs.add(each)
        }
    }
    println("მასივში განსხვავებული ელემენტების რამოდენობა უდრის ${difNumbs.size}")
}

fun msgavseba(numbs: Array<Int>, numbs1: Array<Int>){
    var tanakveta: MutableList<Int> = mutableListOf()

    for (each in numbs){

        if (numbs.isEmpty() || numbs1.isEmpty()){
            println("ერთერთი მასივი ცარიელია!")
        }
        else if (each in numbs1){
            tanakveta.add(each)
        }
    }
    println("მასივების თანაკვეთაა $tanakveta")
}

fun gaertianeba(numbs: Array<Int>, numbs1: Array<Int>){
    var gaerti: MutableList<Int> = mutableListOf()

    for (each in numbs){
        gaerti.add(each)

    }
    for (each in numbs1){
        if (each !in numbs){
            gaerti.add(each)
        }
    }
    println("მასივების გაერთიანებაა $gaerti")
}

fun meotxeDav(numbs: Array<Int>){
    var sashzeNaklebi: MutableList<Int> = mutableListOf()

    for (each in numbs){
        if (each <= (numbs.sum() / numbs.size)){
            sashzeNaklebi.add(each)
        }
    }
    println("საშუალოზე ნაკლები რიცხვები იქნება $sashzeNaklebi")
}

fun mexuteDav(numbs: Array<Int>){
    var max1: MutableList<Int> = mutableListOf()
    var allNumbs: MutableList<Int> = mutableListOf()
    var min1: MutableList<Int> = mutableListOf()

    for (each in numbs){
        if (max1.isEmpty()){
            max1.add(each)
        }

        else if(max1[0] < each){
            max1[0]= each
        }
        if (min1.isEmpty()){
            min1.add(each)
        }
        else if (each < min1[0]){
            min1[0] = each
        }
        allNumbs.add(each)
    }

    allNumbs.remove(max1[0])
    allNumbs.remove(min1[0])

    println(allNumbs)
// dro ar meyo tore mivxvdi rogorc udna meqna :(

}
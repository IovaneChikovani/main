package com.example.assignment1

import kotlin.math.min
import kotlin.math.max
import kotlin.reflect.typeOf


fun main(){

    val summon = actions()

    summon.usj(8,4)
    summon.check_str("iyoda arayio $")
    summon.sum_even_to_100()
    summon.reverse_number(129329)
    summon.check_ifpalindrome("civic")
}

class actions(){

    fun usg(numb1:Int, numb2: Int){

        val min_numb = min(numb1,numb2)

        for (usg in min_numb downTo 0) {

            if (numb1 % usg == 0 && numb2 % usg == 0) {
                println("ricxvebis usg aris $usg")
                break
            }

            else if(usg == 1){
                println("ricxvebis usg aris 1")
                break
            }
        }
    }

    fun usj(numb1: Int, numb2: Int){
        var max_numb = max(numb1,numb2)

        while (max_numb>0){

            if (max_numb % numb1 == 0 && max_numb % numb2 == 0){

                println("ricxvebis usj aris $max_numb")
                break
            }
            else max_numb += 1
        }
    }

    fun check_str(word:String){

        for (letter in 0..word.length){

            if(word[letter] == '$'){

                println("sityva sheicavs $ nishans me-${letter+1} adgilas")
                break

            }
            else if (letter == word.length - 1){

                println("sityva ar sheicavs $ nishans")
                break

            }
        }
    }

    fun sum_even_to_100(){
        var sum:Int = 0

        for(numb in 0..99){
            if (numb % 2 == 0){
                sum += numb
            }
        }
        println("yvela luwi ricxvis jami 100-mde = $sum")
    }

    fun reverse_number(numb:Int){

        val numb_tostr:String = numb.toString()
        var reversed_numb = ""

        for (each in numb_tostr.length - 1 downTo 0){

            reversed_numb += numb_tostr[each]

            if (numb_tostr.length == reversed_numb.length){
                println("$numb shebrunebuli ricxvia ${reversed_numb.toInt()}")
            }
        }
    }

    fun check_ifpalindrome(word: String){

        var reversed_word = ""

        for (letter in word.length - 1 downTo 0 ){
            reversed_word += word[letter]

            if (reversed_word == word){
                println("$word aris palindromi")
            }

            else if(letter == 0) println("$word ar aris palindromi")
        }
    }
}